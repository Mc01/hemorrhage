package rainhead
{
	import rainhead.actors.Points;
	import rainhead.events.ChangeScreen;
	import rainhead.screens.AboutScreen;
	import rainhead.screens.GameScreen;
	import rainhead.screens.MenuScreen;
	import rainhead.screens.PortalScreen;
	import rainhead.screens.StoreScreen;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Game extends Sprite
	{		
		private var progress:int = 0;
		private var bank:int = 0;
		private var _Canvas:Quad;
		private var _GameScreen:GameScreen;
		private var _PortalScreen:PortalScreen;
		private var _AboutScreen:AboutScreen;
		private var _StoreScreen:StoreScreen;
		private var _MenuScreen:MenuScreen;
		private var _Points:Points;
		
		public function Game() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			addEventListener(ChangeScreen.CHANGE_SCREEN, onChangeScreen);
			
			_Canvas = new Quad(800, 600, 0xf2f3b3);
			addChild(_Canvas);
			
			_GameScreen = new GameScreen;
			addChild(_GameScreen);
			_GameScreen.disposeGame();
			
			_PortalScreen = new PortalScreen;
			addChild(_PortalScreen);
			_PortalScreen.disposePortal();
			
			_AboutScreen = new AboutScreen;
			addChild(_AboutScreen);
			_AboutScreen.disposeAbout();
			
			_StoreScreen = new StoreScreen;
			addChild(_StoreScreen);
			_StoreScreen.disposeStore();
			
			_MenuScreen = new MenuScreen;
			addChild(_MenuScreen);
			_MenuScreen.initMenu();
			
			_Points = new Points;
			addChild(_Points);
			_Points.initPoints(bank);
		}
		
		private function onChangeScreen(e:ChangeScreen):void 
		{
			switch (e.screen.id) 
			{
				case "about":
					_Points.disposePoints();
					_MenuScreen.disposeMenu();
					_AboutScreen.initAbout();					
				break;
				case "rebout":
					_AboutScreen.disposeAbout();
					_MenuScreen.initMenu();
					_Points.initPoints(bank);
				break;
				case "store":
					_Points.disposePoints();
					_MenuScreen.disposeMenu();
					_StoreScreen.initStore(bank);
				break;
			case "restore":
					bank = e.money;
					
					_StoreScreen.disposeStore();
					_GameScreen.Mode = _StoreScreen.Equip;
					_MenuScreen.initMenu();
					_Points.initPoints(bank);
				break;
				case "portal":
					_Points.disposePoints();
					_MenuScreen.disposeMenu();
					_PortalScreen.initPortal(progress);
				break;
				case "return":
					_PortalScreen.disposePortal();
					_MenuScreen.initMenu();
					_Points.initPoints(bank);
				break;
				case "stroke":
					_PortalScreen.disposePortal();
					_GameScreen.initGame("stroke");
				break;
				case "void":
					_PortalScreen.disposePortal();
					_GameScreen.initGame("void");
				break;
				case "frozen":
					_PortalScreen.disposePortal();
					_GameScreen.initGame("frozen");
				break;
				case "menu":
					bank += e.money;
					
					if (e.progress != 0 && e.progress != progress) 
					{
						progress = e.progress;
					}
					
					_GameScreen.disposeGame();
					_MenuScreen.initMenu();
					_Points.initPoints(bank);
				break;
			}
		}
	}

}