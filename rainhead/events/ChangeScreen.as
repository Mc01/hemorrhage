package rainhead.events 
{
	import starling.events.Event;
	
	public class ChangeScreen extends Event 
	{
		public static const CHANGE_SCREEN:String = "changeScreen";
		
		private var checkMoney:int;
		private var checkProgress:int;
		
		public function ChangeScreen(type:String, bubbles:Boolean=false, data:Object=null, money:int=0, progress:int=0) 
		{
			super(type, bubbles, data);
			checkMoney = money;
			checkProgress = progress;
		}
		
		public function get screen():Object 
		{ 
			return data;
		}
		
		public function get money():int 
		{
			return checkMoney;
		}
		
		public function get progress():int 
		{
			return checkProgress;
		}
	}

}