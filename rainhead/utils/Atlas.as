package rainhead.utils 
{
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Atlas 
	{		
		//Font
		[Embed(source = "../../../lib/font/refont.png")]
		public static const FontPNG:Class;
		[Embed(source = "../../../lib/font/refont.fnt", mimeType = "application/octet-stream")]
		public static const FontXML:Class;
		
		//Menu
		[Embed(source = "../../../lib/menu/aboutAtlas.png")]
		public static const AboutPNG:Class;
		[Embed(source = "../../../lib/menu/aboutAtlas.xml", mimeType = "application/octet-stream")]
		public static const AboutXML:Class;
		
		//Game
		[Embed(source = "../../../lib/game/fade/fadeAtlas.png")]
		public static const FadePNG:Class;
		[Embed(source = "../../../lib/game/fade/fadeAtlas.xml", mimeType = "application/octet-stream")]
		public static const FadeXML:Class;
		[Embed(source = "../../../lib/game/umbrella/umbrellaAtlas.png")]
		public static const UmbrellaPNG:Class;
		[Embed(source = "../../../lib/game/umbrella/umbrellaAtlas.xml", mimeType = "application/octet-stream")]
		public static const UmbrellaXML:Class;
		[Embed(source = "../../../lib/game/impulse/impulseAtlas.png")]
		public static const ImpulsePNG:Class;
		[Embed(source = "../../../lib/game/impulse/impulseAtlas.xml", mimeType = "application/octet-stream")]
		public static const ImpulseXML:Class;
		
		//Hero
		[Embed(source = "../../../lib/hero/heroAtlas.png")]
		public static const HeroPNG:Class;
		[Embed(source = "../../../lib/hero/heroAtlas.xml", mimeType = "application/octet-stream")]
		public static const HeroXML:Class;
		
		//Droplets
		[Embed(source = "../../../lib/drops/dropAtlas.png")]
		public static const DropPNG:Class;
		[Embed(source = "../../../lib/drops/dropAtlas.xml", mimeType = "application/octet-stream")]
		public static const DropXML:Class;
		
		private static var aboutAtlas:TextureAtlas;
		private static var fadeAtlas:TextureAtlas;
		private static var umbrellaAtlas:TextureAtlas;
		private static var impulseAtlas:TextureAtlas;
		private static var heroAtlas:TextureAtlas;
		private static var dropAtlas:TextureAtlas;
		private static var _Drops:Array;
		
		public function Atlas() 
		{
			//Hello!
		}
		
		public static function dropTexture(command:String):Texture 
		{
			if (_Drops == null)
			{
				_Drops = new Array();
				_Drops.push(getAtlas("drops").getTexture("red"));
				_Drops.push(getAtlas("drops").getTexture("blue"));
				_Drops.push(getAtlas("drops").getTexture("black"));
				_Drops.push(getAtlas("drops").getTexture("star"));
			}
			
			if (command == "red") 
			{
				return _Drops[0];
			}
			else if (command == "blue") 
			{
				return _Drops[1];
			}
			else if (command == "black") 
			{
				return _Drops[2];
			}
			else if (command == "star")
			{
				return _Drops[3];
			}
			else return null;
		}
		
		public static function getAtlas(command:String):TextureAtlas
		{
			var texture:Texture;
			var xml:XML;
			
			if (command == "menu") 
			{
				if (aboutAtlas == null)
				{
					texture = getTexture(AboutPNG);
					xml = XML(new AboutXML);
					aboutAtlas = new TextureAtlas(texture, xml);
				}

				return aboutAtlas;
			}
			else if (command == "fade") 
			{
				if (fadeAtlas == null)
				{
					texture = getTexture(FadePNG);
					xml = XML(new FadeXML);
					fadeAtlas = new TextureAtlas(texture, xml);
				}

				return fadeAtlas;
			}
			else if (command == "umbrella") 
			{
				if (umbrellaAtlas == null)
				{
					texture = getTexture(UmbrellaPNG);
					xml = XML(new UmbrellaXML);
					umbrellaAtlas = new TextureAtlas(texture, xml);
				}
				
				return umbrellaAtlas;
			}
			else if (command == "impulse") 
			{
				if (impulseAtlas == null)
				{
					texture = getTexture(ImpulsePNG);
					xml = XML(new ImpulseXML);
					impulseAtlas = new TextureAtlas(texture, xml);
				}
				
				return impulseAtlas;
			}
			else if (command == "hero") 
			{
				if (heroAtlas == null)
				{
					texture = getTexture(HeroPNG);
					xml = XML(new HeroXML);
					heroAtlas = new TextureAtlas(texture, xml);
				}

				return heroAtlas;
			}
			else if (command == "drops") 
			{
				if (dropAtlas == null) 
				{
					texture = getTexture(DropPNG);
					xml = XML(new DropXML);
					dropAtlas = new TextureAtlas(texture, xml);
				}
				
				return dropAtlas;
			}
			else return null;
		}
		
		public static function getTexture(Type:Class):Texture
		{
			return Texture.fromBitmap(new Type);
		}
		
		public static function getFont():BitmapFont
		{
			var Font:BitmapFont = new BitmapFont(getTexture(FontPNG), XML(new FontXML));
			TextField.registerBitmapFont(Font);
			
			return Font;
		}
	}

}