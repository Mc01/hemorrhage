package rainhead.utils 
{
	import starling.display.DisplayObject;
	
	public class Pool 
	{
		private var _Pool:Array;
		private var capacity:int;
		
		public function Pool(type:Class, length:int) 
		{
			_Pool = new Array;
			capacity = length;
			
			for (var i:int = 0; i < length; i++) 
			{
				_Pool[i] = new type; 
			}
		}
		
		public function checkOut():DisplayObject 
		{
			if (capacity > 0) return _Pool[--capacity];
			else throw new Error("End of capacity.");
		}
		
		public function checkIn(o:DisplayObject):void 
		{
			_Pool[capacity++] = o;
		}
	}

}