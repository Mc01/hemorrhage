package rainhead
{
	import flash.display.Sprite;
	import flash.events.Event;
	import starling.core.Starling;
	
	[SWF(frameRate="60", width="800", height="600", backgroundColor="0x333333")]
	public class Main extends Sprite 
	{
		private var _Starling:Starling;
		
		public function Main():void 
		{
			if (stage) initSelf();
			else addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);

			_Starling = new Starling(Game, this.stage);
			_Starling.start();
			_Starling.showStatsAt("left", "top");
		}
	}
}