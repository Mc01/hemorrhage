package rainhead.screens 
{
	import rainhead.events.ChangeScreen;
	import rainhead.utils.Atlas;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class PortalScreen extends Sprite 
	{
		private var portalStroke:Button;
		private var portalVoid:Button;
		private var portalFrozen:Button;
		private var returnButton:Button;
		private var chooseSprite:Image;
		private var progress:int = 0;
		
		public function PortalScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{			
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			portalStroke = new Button(Atlas.getAtlas("fade").getTexture("portalStrokePNG"));
			portalStroke.x = 300 - portalStroke.width - 10;
			portalStroke.y = 40;
			addChild(portalStroke);
			
			portalFrozen = new Button(Atlas.getAtlas("umbrella").getTexture("portalFrozenOFF"));
			portalFrozen.x = ((800 - portalFrozen.width) >> 1) - 15;
			portalFrozen.y = 45;
			addChild(portalFrozen);
			
			portalVoid = new Button(Atlas.getAtlas("impulse").getTexture("portalVoidOFF"));
			portalVoid.x = 500 - 10;
			portalVoid.y = 45;
			addChild(portalVoid);
			
			returnButton = new Button(Atlas.getAtlas("menu").getTexture("return"));
			returnButton.x = portalVoid.x + 25;
			returnButton.y = 445;
			addChild(returnButton);
			
			chooseSprite = new Image(Atlas.getAtlas("menu").getTexture("choose"));
			chooseSprite.x = portalStroke.x + 10;
			chooseSprite.y = 420;
			addChild(chooseSprite);
		}
		
		public function disposePortal():void 
		{
			removeEventListener(Event.TRIGGERED, onTrigger);
			
			this.visible = false;
			//trace("Portal screen is disposed.");
		}
		
		public function initPortal(pro:int = 0):void 
		{
			if (progress != pro && pro == 1) 
			{
				portalFrozen.changeTexture(Atlas.getAtlas("umbrella").getTexture("portalFrozenPNG"));
				progress = pro;
			}
			else if (progress != pro && pro == 2) 
			{
				portalVoid.changeTexture(Atlas.getAtlas("impulse").getTexture("portalVoidPNG"));
				progress = pro;
			}
			
			this.visible = true;
			//trace("Portal screen is visible.");
			
			addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			if((buttonClicked as Button) == portalStroke)
			{
				this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "stroke" }));
			}
			else if((buttonClicked as Button) == portalFrozen)
			{
				if (progress >= 1) 
				{
					this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "frozen" }));
				}
			}
			else if((buttonClicked as Button) == portalVoid)
			{
				if (progress >= 2) 
				{
					this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "void" }));
				}
			}
			else if((buttonClicked as Button) == returnButton)
			{
				this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "return" }));
			}
		}
	}

}