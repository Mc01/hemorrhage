package rainhead.screens 
{
	import flash.crypto.generateRandomBytes;
	import flash.events.TimerEvent;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	import rainhead.actors.Droplet;
	import rainhead.actors.Fore;
	import rainhead.actors.Hero;
	import rainhead.actors.Stage;
	import rainhead.events.ChangeScreen;
	import rainhead.utils.Atlas;
	import rainhead.utils.Pool;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class GameScreen extends Sprite 
	{
		private var _Canvas:Quad;
		private var _Fore:Fore;
		private var _Stage:Stage;
		private var _Hero:Hero; 
		private var _Pool:Pool;
		private var _Drops:Array;
		private var _Droplet:Droplet;
		private var dropletTimer:Timer;
		private var freezeTimer:Timer;
		private var fadeMulti:Number;
		private var impulseSize:Number;
		private var speedFPS:Number;
		private var touchPoint:Number;
		private var bGrounded:Boolean = true;
		private var bTick:Boolean = false;
		private var bUmbrella:Boolean = false;
		private var modeLock:Boolean = false;
		private var bMode:String = null;
		private var sublevel:String = null;
		private var randPos:ByteArray;
		private var startPos:int;
		private var bankMoney:int;
		private var bankPathogen:int;
		private var pointSprite:Image;
		private var powerSprite:Image;
		private var to5:Image;
		private var pointField:TextField;	
		private var powerField:TextField;
		
		public function GameScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			_Canvas = new Quad(800, 600, 0xf2f3b3);
			addChild(_Canvas);
			
			_Fore = new Fore();
			addChild(_Fore);
			_Fore.y = 570 - _Fore.height;
			
			_Hero = new Hero();
			_Hero.touchable = false;
			addChild(_Hero);
			startPos = 800 - _Hero.x >> 1;
			
			_Hero.x = startPos;
			_Hero.y = 525;
			
			_Stage = new Stage();
			_Stage.touchable = false;
			addChild(_Stage);
			_Stage.y = 600 - _Stage.height;
			
			pointSprite = new Image(Atlas.getAtlas("menu").getTexture("points"));
			pointSprite.x = 40;
			pointSprite.y = 30;
			addChild(pointSprite);
			
			pointField = new TextField(300, 100, "0", Atlas.getFont().name, 36, 0xffffff);
			pointField.hAlign = HAlign.LEFT;
			pointField.vAlign = VAlign.TOP;
			pointField.x = pointSprite.x + pointSprite.width;
			pointField.y = pointSprite.y - 8;
			addChild(pointField);
			
			powerSprite = new Image(Atlas.getAtlas("menu").getTexture("power"));
			powerSprite.x = pointSprite.x;
			powerSprite.y = pointSprite.y + pointSprite.height + 15;
			
			powerField = new TextField(300, 100, "0", Atlas.getFont().name, 36, 0xffffff);
			powerField.hAlign = HAlign.LEFT;
			powerField.vAlign = VAlign.TOP;
			powerField.x = powerSprite.x + powerSprite.width;
			powerField.y = powerSprite.y - 8;
			
			to5 = new Image(Atlas.getAtlas("menu").getTexture("to5"));
			to5.x = powerSprite.x + powerSprite.width + 45;
			to5.y = powerSprite.y;
			
			_Pool = new Pool(Droplet, 20);
			_Drops = new Array();
			
			dropletTimer = new Timer(750, 0);
			freezeTimer = new Timer(2000, 1);			
		}
		
		public function disposeGame():void 
		{
			removeEventListener(TouchEvent.TOUCH, onTouch);
			removeEventListener(Event.ENTER_FRAME, onTick);
			bTick = false;
			dropletTimer.stop();
			dropletTimer.removeEventListener(TimerEvent.TIMER, rainOut);
			
			if (_Drops.length != 0)
			{
				for each (_Droplet in _Drops)
				{
					rainIn();
				}
				
				_Drops.splice(0, _Drops.length);
			}
			
			this.visible = false;
			//trace("Game screen is disposed.");
		}
		
		public function initGame(command:String):void 
		{
			sublevel = command;
			
			if (command == "stroke")
			{
				_Canvas.color = 0xf2f3b3;
			}
			else if (command == "void")
			{
				_Canvas.color = 0xe6c4eb;
			}
			else if (command == "frozen")
			{
				_Canvas.color = 0xc8d4e5;
			}
			
			if (bMode != null && getChildIndex(powerSprite) == -1) 
			{
				addChild(powerSprite);
				addChild(powerField);
				addChild(to5);
				//trace("Invoked!");
			}
			
			_Hero.scaleX = 0.8;
			_Hero.scaleY = 0.8;
			
			modeLock = false;
			fadeMulti = 1.0;
			impulseSize = 0.8;
			bUmbrella = false;
			bankPathogen = 0;
			
			_Fore.ignite(command);
			_Stage.ignite(command);
			
			bankMoney = 0;
			pointField.text = "" + bankMoney;
			powerField.text = "" + bankPathogen;
			
			bGrounded = true;
			_Hero.reinit();
			_Hero.x = startPos;
			
			this.visible = true;
			//trace("Game screen is visible.");
			
			addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function onTouch(e:TouchEvent):void 
		{
			var _Touch:Touch = e.getTouch(stage);
			touchPoint = _Touch.getLocation(stage).x;
			
			if (bGrounded == true)
			{
				bGrounded = false;
				_Hero.takeOff();
			}
			if (bTick == false)
			{
				bTick = true;
				addEventListener(Event.ENTER_FRAME, onTick);
				dropletTimer.addEventListener(TimerEvent.TIMER, rainOut);
				dropletTimer.start();
			}
		}
		
		private function onTick():void 
		{
			speedFPS = 60 / Starling.current.StatsDisp.FPS;
			
			unlockPowers();		
			heroMovement();
			dropletBehaviour();
		}
		
		private function unlockPowers():void 
		{
			if (bMode != null && modeLock == false) 
			{
				if (bankPathogen == 5) 
				{
					if (bMode == "fade") 
					{
						fadeMulti = 3.0;
						_Hero.setFade();
						modeLock = true;
					}
					else if (bMode == "umbrella") 
					{
						bUmbrella = true;
						_Hero.setUmbrella();
						modeLock = true;
					}
					else if (bMode == "impulse") 
					{
						impulseSize = 1.2;
						modeLock = true;
					}
				}
			}
		}
		
		private function heroMovement():void 
		{
			if(_Hero.mirrorAble == false || _Hero.x > touchPoint) _Hero.scaleX = 1 * impulseSize;
			else if (_Hero.x < touchPoint) _Hero.scaleX = -1 * impulseSize;
			_Hero.scaleY = 1 * impulseSize;
			
			_Hero.x -= (_Hero.x - touchPoint) * 0.04 * fadeMulti;
			if (_Hero.x < (0.1 * stage.stageWidth)) _Hero.x = 0.1 * stage.stageWidth;
			if (_Hero.x > (0.9 * stage.stageWidth)) _Hero.x = 0.9 * stage.stageWidth;
		}
		
		private function dropletBehaviour():void 
		{
			for (var i:int = 0; i < _Drops.length; i++) 
			{
				_Drops[i].y += 5 * speedFPS * _Drops[i].Speed;
				
				if (_Hero.bounds.intersects(_Drops[i].bounds))
				{					
					_Droplet = _Drops[i];
					rainIn();
					_Drops.splice(i, 1);
					
					//BLUE
					if (_Droplet.Type == "blue")
					{
						if (bUmbrella == true) 
						{
							bUmbrella = false;
							_Hero.setFlying();
							modeLock = false;
							bankPathogen = 0;
							powerField.text = "" + bankPathogen;
						}
						else 
						{
							removeEventListener(TouchEvent.TOUCH, onTouch);
							bGrounded = true;
							_Hero.freeze();
							freezeTimer.addEventListener(TimerEvent.TIMER, releaseStun);
							freezeTimer.start();
							
							if (fadeMulti == 3.0)
							{
								fadeMulti = 1.0;
								modeLock = false;
								bankPathogen = 0;
								powerField.text = "" + bankPathogen;
							}
							else if (impulseSize == 1.2) 
							{
								impulseSize = 0.8
								modeLock = false;
								bankPathogen = 0;
								powerField.text = "" + bankPathogen;
							}
						}
					}
					//RED
					else if (_Droplet.Type == "red")
					{
						_Hero.blood();
						bankMoney += 10;
						pointField.text = "" + bankMoney;
					}
					//BLACK
					else if (_Droplet.Type == "black")
					{
						_Hero.pathogen();
						bankPathogen += 1;
						if (bankPathogen <= 5) 
						{
							powerField.text = "" + bankPathogen;
						}
					}
					//STAR
					else if (_Droplet.Type == "star")
					{
						if (sublevel == "stroke") 
						{
							this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "menu" }, bankMoney, 1));
							break;
						}
						else if (sublevel == "frozen") 
						{
							this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "menu" }, bankMoney, 2));
							break;
						}
						else if (sublevel == "void") 
						{
							this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "menu" }, bankMoney, 3));
							break;
						}
					}
				}
				//STAGE	
				if (_Stage.bounds.intersects(_Drops[i].bounds))
				{
					_Droplet = _Drops[i];
					rainIn();
					_Drops.splice(i, 1);
					
					if (_Droplet.Type == "black")
					{
						this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "menu" }, bankMoney));
						break;
					}
				}
			}
		}
		
		private function releaseStun(e:TimerEvent):void 
		{
			addEventListener(TouchEvent.TOUCH, onTouch);
			freezeTimer.stop();
			freezeTimer.removeEventListener(TimerEvent.TIMER, releaseStun);
		}
		
		private function rainOut(e:TimerEvent):void 
		{
			_Droplet = _Pool.checkOut() as Droplet;
			randPos = generateRandomBytes(1);
			_Droplet.x = 32 + randPos[0] * 2.5;
			_Droplet.y = -100;
			
			_Droplet.randSelf(sublevel, bankMoney)
			
			_Droplet.reinitSelf();
			addChild(_Droplet);
			_Drops.push(_Droplet);
		}
		
		private function rainIn():void 
		{
			removeChild(_Droplet);
			_Pool.checkIn(_Droplet);
		}

		public function set Mode(mode:String):void 
		{
			bMode = mode;
		}
	}

}