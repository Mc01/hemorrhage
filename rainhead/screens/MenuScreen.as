package rainhead.screens 
{
	import rainhead.events.ChangeScreen;
	import rainhead.utils.Atlas;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	
	public class MenuScreen extends Sprite 
	{
		private var playButton:Button;
		private var aboutButton:Button;
		private var storeButton:Button;
		private var readIt:Image;
		
		public function MenuScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{			
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			playButton = new Button(Atlas.getAtlas("menu").getTexture("play"));
			playButton.x = 800 - playButton.width >> 1;
			playButton.y = 175;
			addChild(playButton);
			
			aboutButton = new Button(Atlas.getAtlas("menu").getTexture("about"));
			aboutButton.x = 800 - aboutButton.width >> 1;
			aboutButton.y = 255;
			addChild(aboutButton);
			
			storeButton = new Button(Atlas.getAtlas("menu").getTexture("store"));
			storeButton.x = 800 - storeButton.width >> 1;
			storeButton.y = 330;
			addChild(storeButton);
			
			readIt = new Image(Atlas.getAtlas("menu").getTexture("readIt"));
			readIt.x = aboutButton.x + aboutButton.width;
			readIt.y = aboutButton.y - 40;
			addChild(readIt);
		}
		
		public function disposeMenu():void 
		{
			removeEventListener(Event.TRIGGERED, onTrigger);
			
			this.visible = false;
			//trace("Menu screen is disposed.");
		}
		
		public function initMenu():void 
		{
			this.visible = true;
			//trace("Menu screen is visible.");
			
			addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			if((buttonClicked as Button) == playButton)
			{
				this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "portal" } ));
			}
			else if ((buttonClicked as Button) == aboutButton)
			{
				this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "about" } ));
			}
			else if ((buttonClicked as Button) == storeButton) 
			{
				this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "store" } ));
			}
		}
	}

}