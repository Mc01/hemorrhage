package rainhead.screens 
{
	import rainhead.events.ChangeScreen;
	import rainhead.utils.Atlas;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class AboutScreen extends Sprite 
	{
		private var aboutSprite:Image;
		private var returnButton:Button;
		
		public function AboutScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{			
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			aboutSprite = new Image(Atlas.getAtlas("menu").getTexture("aboutText"));
			aboutSprite.x = 70;
			aboutSprite.y = 40;
			addChild(aboutSprite);
			
			returnButton = new Button(Atlas.getAtlas("menu").getTexture("return"));
			returnButton.x = 500;
			returnButton.y = 445;
			addChild(returnButton);		
		}
		
		public function disposeAbout():void 
		{
			removeEventListener(Event.TRIGGERED, onTrigger);
			
			this.visible = false;
			//trace("About screen is disposed.");
		}
		
		public function initAbout():void 
		{
			this.visible = true;
			//trace("About screen is visible.");
			
			addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			if((buttonClicked as Button) == returnButton)
			{
				this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "rebout" }));
			}
		}
	}

}