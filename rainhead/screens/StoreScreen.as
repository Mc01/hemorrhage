package rainhead.screens 
{
	import rainhead.events.ChangeScreen;
	import rainhead.utils.Atlas;
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	public class StoreScreen extends Sprite 
	{
		private var buyButton:Button;		
		private var equipButton:Button;
		private var returnButton:Button;
		private var fadeButton:Button;
		private var umbrellaButton:Button;
		private var impulseButton:Button;
		private var bFade:Boolean = false;
		private var bUmbrella:Boolean = false;
		private var bImpulse:Boolean = false;
		private var bSelect:String = null;
		private var bEquip:String = null;
		private var bank:int = 0;
		private var fadeField:TextField;
		private var umbrellaField:TextField;
		private var impulseField:TextField;
		
		public function StoreScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{			
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			fadeButton = new Button(Atlas.getAtlas("fade").getTexture("fadeOFF"));
			fadeButton.x = (800 >> 1) - fadeButton.width - 100;
			fadeButton.y = 80;
			addChild(fadeButton);
			
			umbrellaButton = new Button(Atlas.getAtlas("umbrella").getTexture("umbrellaOFF"));
			umbrellaButton.x = (800 - umbrellaButton.width) >> 1;
			umbrellaButton.y = 85;
			addChild(umbrellaButton);
			
			impulseButton = new Button(Atlas.getAtlas("impulse").getTexture("impulseOFF"));
			impulseButton.x = (800 >> 1) + 110;
			impulseButton.y = 85;
			addChild(impulseButton);
			
			fadeField = new TextField(300, 100, "200", Atlas.getFont().name, 36, 0xffffff);
			fadeField.hAlign = HAlign.LEFT;
			fadeField.vAlign = VAlign.TOP;
			fadeField.x = fadeButton.x + 50;
			fadeField.y = fadeButton.y - 55;
			addChild(fadeField);
			
			umbrellaField = new TextField(300, 100, "400", Atlas.getFont().name, 36, 0xffffff);
			umbrellaField.hAlign = HAlign.LEFT;
			umbrellaField.vAlign = VAlign.TOP;
			umbrellaField.x = umbrellaButton.x + 50;
			umbrellaField.y = umbrellaButton.y - 55;
			addChild(umbrellaField);
			
			impulseField = new TextField(300, 100, "600", Atlas.getFont().name, 36, 0xffffff);
			impulseField.hAlign = HAlign.LEFT;
			impulseField.vAlign = VAlign.TOP;
			impulseField.x = impulseButton.x + 50;
			impulseField.y = impulseButton.y - 55;
			addChild(impulseField);
			
			buyButton = new Button(Atlas.getAtlas("menu").getTexture("buy"));
			buyButton.x = fadeButton.x + 10;
			buyButton.y = 440;
			addChild(buyButton);
			
			equipButton = new Button(Atlas.getAtlas("menu").getTexture("equip"));
			equipButton.x = umbrellaButton.x - 20;
			equipButton.y = 445;
			addChild(equipButton);
			
			returnButton = new Button(Atlas.getAtlas("menu").getTexture("return"));
			returnButton.x = impulseButton.x + (impulseButton.width >> 2) - 10;
			returnButton.y = 445;
			addChild(returnButton);
		}
		
		public function disposeStore():void 
		{
			removeEventListener(Event.TRIGGERED, onTrigger);
			
			this.visible = false;
			//trace("Store screen is disposed.");
		}
		
		public function initStore(bbank:int):void 
		{
			this.visible = true;
			//trace("Store screen is visible.");
			
			bank = bbank;
			
			addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function onTrigger(e:Event):void 
		{
			var buttonClicked:Button = e.target as Button;
			
			if((buttonClicked as Button) == returnButton)
			{
				deselectFade();
				deselectImpulse();
				deselectUmbrella();
				this.dispatchEvent(new ChangeScreen(ChangeScreen.CHANGE_SCREEN, true, { id: "restore" }, bank));
			}
//=== EQUIP
			else if ((buttonClicked as Button) == equipButton)
			{
				if (bSelect == "fade" && bFade == true) 
				{
					bSelect = bEquip;
					if (bEquip == "umbrella") 
					{
						deselectUmbrella();
					}
					else if (bEquip == "impulse") 
					{
						deselectImpulse();
					}
					
					fadeButton.changeTexture(Atlas.getAtlas("fade").getTexture("fadeGOLD"));
					bSelect = null;
					bEquip = "fade";
				}
				else if (bSelect == "umbrella" && bUmbrella == true) 
				{
					bSelect = bEquip;
					if (bEquip == "fade") 
					{
						deselectFade();
					}
					else if (bEquip == "impulse") 
					{
						deselectImpulse();
					}
					
					umbrellaButton.changeTexture(Atlas.getAtlas("umbrella").getTexture("umbrellaGOLD"));
					bSelect = null;
					bEquip = "umbrella";
				}
				else if (bSelect == "impulse" && bImpulse == true) 
				{
					bSelect = bEquip;
					if (bEquip == "fade") 
					{
						deselectFade();
					}
					else if (bEquip == "umbrella") 
					{
						deselectUmbrella();
					}
					
					impulseButton.changeTexture(Atlas.getAtlas("impulse").getTexture("impulseGOLD"));
					bSelect = null;
					bEquip = "impulse";
				}
			}	
//=== BUY
			else if ((buttonClicked as Button) == buyButton) 
			{
				if (bSelect == "fade" && bFade == false && bank >= 200) 
				{
					bank -= 200;
					fadeButton.changeTexture(Atlas.getAtlas("fade").getTexture("fadePNG"));
					bSelect = null;
					bFade = true;
				}
				else if (bSelect == "umbrella" && bUmbrella == false && bank >= 400) 
				{
					bank -= 400;
					umbrellaButton.changeTexture(Atlas.getAtlas("umbrella").getTexture("umbrellaPNG"));
					bSelect = null;
					bUmbrella = true;
				}
				else if (bSelect == "impulse" && bImpulse == false && bank >= 600) 
				{
					bank -= 600;
					impulseButton.changeTexture(Atlas.getAtlas("impulse").getTexture("impulsePNG"));
					bSelect = null;
					bImpulse = true;
				}
			}
//=== SELECT
			else if ((buttonClicked as Button) == fadeButton)
			{
				if (bEquip != "fade") 
				{
					deselectUmbrella();
					deselectImpulse();
					
					if (bSelect == null)
					{
						fadeButton.changeTexture(Atlas.getAtlas("fade").getTexture("fadeRED"));
						bSelect = "fade";
					}
				}
			}
			else if ((buttonClicked as Button) == umbrellaButton)
			{				
				if (bEquip != "umbrella") 
				{
					deselectFade();
					deselectImpulse();
					
					if (bSelect == null)
					{
						umbrellaButton.changeTexture(Atlas.getAtlas("umbrella").getTexture("umbrellaRED"));
						bSelect = "umbrella";
					}
				}
			}
			else if ((buttonClicked as Button) == impulseButton)
			{
				if (bEquip != "impulse") 
				{
					deselectUmbrella(); 
					deselectFade();
					
					if (bSelect == null)
					{
						impulseButton.changeTexture(Atlas.getAtlas("impulse").getTexture("impulseRED"));
						bSelect = "impulse";
					}
				}
			}
		}
		
		private function deselectFade():void 
		{
			if (bSelect == "fade") 
			{
				if (bFade == false)
				{
					fadeButton.changeTexture(Atlas.getAtlas("fade").getTexture("fadeOFF"));
				}
				else 
				{
					fadeButton.changeTexture(Atlas.getAtlas("fade").getTexture("fadePNG"));
				}
				bSelect = null;
			}
		}
		
		private function deselectUmbrella():void 
		{
			if (bSelect == "umbrella") 
			{
				if (bUmbrella == false) 
				{
					umbrellaButton.changeTexture(Atlas.getAtlas("umbrella").getTexture("umbrellaOFF"));
				}
				else 
				{
					umbrellaButton.changeTexture(Atlas.getAtlas("umbrella").getTexture("umbrellaPNG"));
				}
				bSelect = null;
			}
		}
		
		private function deselectImpulse():void 
		{
			if (bSelect == "impulse") 
			{
				if (bImpulse == false) 
				{
					impulseButton.changeTexture(Atlas.getAtlas("impulse").getTexture("impulseOFF"));
				}
				else 
				{
					impulseButton.changeTexture(Atlas.getAtlas("impulse").getTexture("impulsePNG"));
				}
				bSelect = null;
			}
		}
		
		public function get Equip():String
		{
			return bEquip;
		}
	}
}