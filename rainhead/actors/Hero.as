package rainhead.actors 
{
	import com.greensock.easing.Bounce;
	import com.greensock.plugins.HexColorsPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.TweenLite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import mj5.ExtendedMovieClip;
	import rainhead.utils.Atlas;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;

	public class Hero extends Sprite 
	{
		private var _Hero:ExtendedMovieClip;
		private var isflyTimer:Timer;
		private var reviveTimer:Timer;
		private var colorTimer:Timer;
		private var unfreezeTimer:Timer;
		private var bFrozen:Boolean = false;
		private var bMirrorAble:Boolean = false;
		
		public function Hero() 
		{
			addEventListener(Event.ADDED_TO_STAGE, iniSelf);
		}
		
		private function iniSelf():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, iniSelf);
			
			_Hero = new ExtendedMovieClip();
			_Hero.addAnimation("idle", Atlas.getAtlas("hero").getTextures("idle"), 1, false);
			_Hero.addAnimation("jump", Atlas.getAtlas("hero").getTextures("jump_"), 4);
			_Hero.addAnimation("fly", Atlas.getAtlas("hero").getTextures("fly"), 1, false);
			_Hero.addAnimation("umbrella", Atlas.getAtlas("hero").getTextures("umbrella"), 1, false);
			_Hero.addAnimation("fade", Atlas.getAtlas("hero").getTextures("fade"), 1, false);
			
			_Hero.height = 96;
			_Hero.width = 48;
			_Hero.pivotX = _Hero.width >> 1;
			_Hero.pivotY = _Hero.height;
			
			addChild(_Hero);
			Starling.juggler.add(_Hero);
			
			isflyTimer = new Timer(500, 1);
			colorTimer = new Timer(500, 1);
			unfreezeTimer = new Timer(500, 1);
			reviveTimer = new Timer(1500, 1);
			
			TweenPlugin.activate([HexColorsPlugin]);
		}
		
		public function reinit():void 
		{
			_Hero.setCurrentAnimation("idle");
			_Hero.y = 0;
			bMirrorAble = false;
			bFrozen = false;
		}
		
		public function takeOff():void 
		{
			TweenLite.to(_Hero, .5, { y: -30 } );
			isflyTimer.addEventListener(TimerEvent.TIMER, isFly);
			isflyTimer.start();
			bMirrorAble = true;
			_Hero.setCurrentAnimation("jump");
			_Hero.play();
		}
		
		public function setUmbrella():void 
		{
			_Hero.setCurrentAnimation("umbrella");
		}
		
		public function setFade():void 
		{
			_Hero.setCurrentAnimation("fade");
		}
		
		public function setFlying():void 
		{
			_Hero.setCurrentAnimation("fly");
		}
		
		public function blood():void 
		{
			if (bFrozen == false)
			{
				TweenLite.to(_Hero, .5, { hexColors: { Color:0xdc896c }} );
				colorTimer.addEventListener(TimerEvent.TIMER, recolor);
				colorTimer.start();
			}
		}
		
		public function pathogen():void 
		{
			if (bFrozen == false) 
			{
				TweenLite.to(_Hero, .5, { hexColors: { Color:0xaa9e73 }} );
				colorTimer.addEventListener(TimerEvent.TIMER, recolor);
				colorTimer.start();
			}
		}
		
		public function freeze():void 
		{
			bFrozen = true;
			TweenLite.killTweensOf(_Hero);
			TweenLite.to(_Hero, 1.5, { y:0, ease:Bounce.easeOut, hexColors: { Color:0xb2bac0 }} );
			bMirrorAble = false;
			_Hero.setCurrentAnimation("idle");
			reviveTimer.addEventListener(TimerEvent.TIMER, revive);
			reviveTimer.start();
		}
		
		private function isFly(e:TimerEvent):void 
		{
			_Hero.stop();
			isflyTimer.stop();
			isflyTimer.removeEventListener(TimerEvent.TIMER, isFly);
			setFlying();
		}
		
		
		private function revive(e:TimerEvent):void 
		{
			TweenLite.to(_Hero, .5, { hexColors: { Color:0xffffff }} );
			reviveTimer.stop();
			reviveTimer.removeEventListener(TimerEvent.TIMER, revive);
			
			unfreezeTimer.addEventListener(TimerEvent.TIMER, unfreeze);
			unfreezeTimer.start();
		}
		
		private function unfreeze(e:TimerEvent):void 
		{
			if (bFrozen == true) bFrozen = false;
			unfreezeTimer.stop();
			unfreezeTimer.removeEventListener(TimerEvent.TIMER, unfreeze);
		}
		
		private function recolor(e:TimerEvent):void 
		{
			if(bFrozen == false) TweenLite.to(_Hero, .5, { hexColors: { Color:0xffffff }} );
			colorTimer.stop();
			colorTimer.removeEventListener(TimerEvent.TIMER, recolor);
		}
		
		public function get mirrorAble():Boolean 
		{
			return bMirrorAble;
		}
	}

}