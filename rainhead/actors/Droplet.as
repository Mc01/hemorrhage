package rainhead.actors 
{
	import flash.crypto.generateRandomBytes;
	import flash.utils.ByteArray;
	import rainhead.utils.Atlas;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Droplet extends Sprite 
	{
		private var _Droplet:Image;
		private var randType:String;
		private var randNumb:ByteArray;
		private var speedType:Number;
		
		public function Droplet() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			randSelf();
			if (randType != null)
			{
				_Droplet = new Image(Atlas.dropTexture(randType));
				_Droplet.readjustSize();
				
				addChild(_Droplet);
			}
		}
		
		public function reinitSelf():void 
		{
			if (_Droplet != null) 
			{
				_Droplet.texture = Atlas.dropTexture(randType);
				_Droplet.readjustSize();
			}
		}
				
		public function randSelf(sublevel:String = "stroke", bankMoney:int = 0):void
		{
			randNumb = generateRandomBytes(1);
			
			if (sublevel == "stroke") 
			{
				if (bankMoney < 80) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 155)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 156 && randNumb[0] <= 205) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 206 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 0.6;
					}
				}
				else if (bankMoney >= 80 && bankMoney < 140) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 105)
					{
						randType = "red";
						speedType = 1.3;
					}
					else if (randNumb[0] >= 106 && randNumb[0] <= 180) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 181 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 0.6;
					}					
				}
				else if (bankMoney >= 140 && bankMoney < 200) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 55)
					{
						randType = "red";
						speedType = 1.6;
					}
					else if (randNumb[0] >= 56 && randNumb[0] <= 155) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 156 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 0.6;
					}
				}
				else if (bankMoney >= 200) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 55)
					{
						randType = "red";
						speedType = 0.6;
					}
					else if (randNumb[0] >= 56 && randNumb[0] <= 200) 
					{
						randType = "black";
						speedType = 0.6;
					}
					else if (randNumb[0] >= 201 && randNumb[0] <= 255) 
					{
						randType = "star";
						speedType = 0.1;
					}
				}
			}
			else if (sublevel == "frozen") 
			{
				if (bankMoney < 100) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 105)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 106 && randNumb[0] <= 210) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 211 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 0.6;
					}
				}
				else if (bankMoney >= 100 && bankMoney < 200) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 75)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 76 && randNumb[0] <= 210) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 211 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 0.8;
					}					
				}
				else if (bankMoney >= 200 && bankMoney < 320) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 50)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 51 && randNumb[0] <= 185) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 186 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 1.0;
					}
				}
				else if (bankMoney >= 320 && bankMoney < 400) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 25)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 26 && randNumb[0] <= 185) 
					{
						randType = "blue";
						speedType = 1.6;
					}
					else if (randNumb[0] >= 186 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 1.0;
					}
				}
				else if (bankMoney >= 400)
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 25)
					{
						randType = "red";
						speedType = 0.6;
					}
					else if (randNumb[0] >= 26 && randNumb[0] <= 125) 
					{
						randType = "blue";
						speedType = 1.6;
					}
					else if (randNumb[0] >= 126 && randNumb[0] <= 225) 
					{
						randType = "black";
						speedType = 0.6;
					}
					else if (randNumb[0] >= 226 && randNumb[0] <= 255) 
					{
						randType = "star";
						speedType = 0.1;
					}
				}
			}
			else if (sublevel == "void") 
			{
				if (bankMoney < 120) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 105)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 106 && randNumb[0] <= 155) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 156 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 0.6;
					}
				}
				else if (bankMoney >= 120 && bankMoney < 240) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 105)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 106 && randNumb[0] <= 150) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 151 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 0.8;
					}					
				}
				else if (bankMoney >= 240 && bankMoney < 480) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 75)
					{
						randType = "red";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 76 && randNumb[0] <= 125) 
					{
						randType = "blue";
						speedType = 1.0;
					}
					else if (randNumb[0] >= 126 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 1.0;
					}
				}
				else if (bankMoney >= 480 && bankMoney < 600) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 55)
					{
						randType = "red";
						speedType = 1.6;
					}
					else if (randNumb[0] >= 56 && randNumb[0] <= 110) 
					{
						randType = "blue";
						speedType = 1.6;
					}
					else if (randNumb[0] >= 111 && randNumb[0] <= 255) 
					{
						randType = "black";
						speedType = 1.6;
					}
				}
				else if (bankMoney >= 600) 
				{
					if (randNumb[0] >= 0 && randNumb[0] <= 25)
					{
						randType = "red";
						speedType = 0.6;
					}
					else if (randNumb[0] >= 26 && randNumb[0] <= 125) 
					{
						randType = "blue";
						speedType = 0.6
					}
					else if (randNumb[0] >= 126 && randNumb[0] <= 245) 
					{
						randType = "black";
						speedType = 0.6;
					}
					else if (randNumb[0] >= 246 && randNumb[0] <= 255) 
					{
						randType = "star";
						speedType = 0.1;
					}
				}
			}
		}
		
		public function get Type():String 
		{
			return randType;
		}
		
		public function get Speed():Number 
		{
			return speedType;
		}
	}

}