package rainhead.actors 
{
	import rainhead.utils.Atlas;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class Stage extends Sprite 
	{
		private var _Stage:Image;
		private var _stroke:Texture;
		private var _void:Texture;
		private var _frozen:Texture;
		
		public function Stage() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf():void 
		{			
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			_stroke = Atlas.getAtlas("fade").getTexture("strokeStage");
			_void = Atlas.getAtlas("impulse").getTexture("voidStage");
			_frozen = Atlas.getAtlas("umbrella").getTexture("frozenStage");
			
			_Stage = new Image(_stroke);
			addChild(_Stage);
		}
		
		public function ignite(command:String):void
		{
			if (command == "stroke")
			{
				_Stage.texture = _stroke;
				_Stage.readjustSize();
			}
			else if (command == "void")
			{
				_Stage.texture = _void;
				_Stage.readjustSize();
			}
			else if (command == "frozen")
			{
				_Stage.texture = _frozen;
				_Stage.readjustSize();
			}
		}
	}

}