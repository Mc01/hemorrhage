package rainhead.actors 
{
	import rainhead.utils.Atlas;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class Points extends Sprite 
	{
		private var pointSprite:Image;
		private var pointField:TextField;
		
		public function Points() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			pointSprite = new Image(Atlas.getAtlas("menu").getTexture("points"));
			pointSprite.x = 40;
			pointSprite.y = 30;
			addChild(pointSprite);
			
			pointField = new TextField(300, 100, "0", Atlas.getFont().name, 36, 0xffffff);
			pointField.hAlign = HAlign.LEFT;
			pointField.vAlign = VAlign.TOP;
			pointField.x = 40 + pointSprite.width;
			pointField.y = 22;
			addChild(pointField);
		}
		
		public function disposePoints():void 
		{
			this.visible = false;
		}
		
		public function initPoints(score:int):void 
		{
			pointField.text = "" + score;
			this.visible = true;
		}
		
		
	}

}