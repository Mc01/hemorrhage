package rainhead.actors 
{
	import rainhead.utils.Atlas;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class Fore extends Sprite 
	{
		private var _Fore:Image;
		private var _stroke:Texture;
		private var _void:Texture;
		private var _frozen:Texture;
		
		public function Fore() 
		{
			addEventListener(Event.ADDED_TO_STAGE, initSelf);
		}
		
		private function initSelf():void 
		{			
			removeEventListener(Event.ADDED_TO_STAGE, initSelf);
			
			_stroke = Atlas.getAtlas("fade").getTexture("strokeFore");
			_void = Atlas.getAtlas("impulse").getTexture("voidFore");
			_frozen = Atlas.getAtlas("umbrella").getTexture("frozenFore");
			
			_Fore = new Image(_stroke);
			addChild(_Fore);
		}

		public function ignite(command:String):void
		{
			if (command == "stroke")
			{
				_Fore.texture = _stroke;
				_Fore.readjustSize();
			}
			else if (command == "void")
			{
				_Fore.texture = _void;
				_Fore.readjustSize();
			}
			else if (command == "frozen")
			{
				_Fore.texture = _frozen;
				_Fore.readjustSize();
			}
		}
	}
}